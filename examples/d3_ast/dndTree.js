// set up some viewer size info
var viewerWidth  = document.body.offsetWidth;
var viewerHeight = document.body.offsetHeight;

var duration = 500;

var horiz_spacing = 100;

var margin = {top: 20, right: 120, bottom: 20, left: 120},
width = viewerWidth - margin.right - margin.left,
height = viewerHeight - margin.top - margin.bottom;

var i = 0;

// the tree function from d3 that does some of the crux of the work
var tree = d3.layout.tree()
    .size([height, width]);


// diagonal is responsible for drawing links between nodes
var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });


// create the svg with a g element fitting it to the screen
var svg = d3.select("#tree-container").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var drawTree = function(root) {

    // draw the tree
    update(root);

    function update(source) {

        //                                           _
        //  _ __ ___  ___ ___  _ __ ___  _ __  _   _| |_ ___
        // | '__/ _ \/ __/ _ \| '_ ` _ \| '_ \| | | | __/ _ \
        // | | |  __/ (_| (_) | | | | | | |_) | |_| | ||  __/
        // |_|  \___|\___\___/|_| |_| |_| .__/ \__,_|\__\___|
        //                              |_|

        // recompute tree layout
        var nodes = tree.nodes(source).reverse();
        var links = tree.links(nodes);

        // update all nodes and links (using the precomputed unique ids)
        var node = svg.selectAll("g.node")
            .data(nodes, function(d) { return d.myid; });
        var link = svg.selectAll("path.link")
            .data(links, function(d) { return d.target.myid; });





        //             _
        //   ___ _ __ | |_ ___ _ __
        //  / _ \ '_ \| __/ _ \ '__|
        // |  __/ | | | ||  __/ |
        //  \___|_| |_|\__\___|_|

        // for each *new* node, create a SVG group ...
        var enteringNodes = node.enter().append("g")
            .attr("class", "node")

        // ... and add a circle  ...
        enteringNodes.append("circle")
            .attr("r", 10)
            .style("fill", "#fff");

        // ... and add a text label ...
        enteringNodes.append("text")
            .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
            .attr("dy", ".35em")
            .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
            .text(function(d) { return d.name; })
            .style("fill-opacity", 1);

        // ... and transition it to its new location.
        enteringNodes
            // .transition()
            // .duration(duration)
            .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

        // for each *new* link, create the appropriate SVG elements
        var enteringLinks = link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", diagonal);




        //                  _       _
        //  _   _ _ __   __| | __ _| |_ ___
        // | | | | '_ \ / _` |/ _` | __/ _ \
        // | |_| | |_) | (_| | (_| | ||  __/
        //  \__,_| .__/ \__,_|\__,_|\__\___|
        //       |_|

        // transition all existing nodes to their (recomputed) locations
        node
            .transition()
            .duration(duration)
            .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
            .style("fill-opacity", 1);


        // transition all links to their (recomputed) locations
        link
            .transition()
            .duration(duration)
            .attr("class", "link")
            .attr("d", diagonal);




        //            _ _
        //   _____  _(_) |_
        //  / _ \ \/ / | __|
        // |  __/>  <| | |_
        //  \___/_/\_\_|\__|

        // remove all nodes that no longer exist.
        node.exit().remove();
        link.exit().remove();

        

    }
}