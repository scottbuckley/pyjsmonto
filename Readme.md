PyJSMonto
=========
PyJSMonto is a broker that allows for [Monto](https://bitbucket.org/inkytonik/monto) sinks in a web browser (or any platform that supports [WebSockets](https://developer.mozilla.org/en-US/docs/WebSockets)).

PyJSMonto is implemented as a Monto sink which forwards products over a WebSocket connection.


Settings
--------
By default, PyJSMonto will forward any products with the languages `json`, `markdown`, or `html`. To change this behaviour, modify the following line at the top of `pyjsmonto.py`:

~~~
#!javascript
languagesToPassOn = ["json", "markdown", "html"]
~~~

Add or remove any product languages as necessary, or provide an empty list to allow all products to be forwarded.

By default, port `9999` will be used for WebSocket connections, but this can be changed by altering the global `WS_PORT` variable at the top of `pyjsmonto.py`.


Usage
-----
To forward monto products to WebSocket sinks, simply run `pyjsmonto.py` anywhere you would run a standard monto sink.

To receive monto products via WebSockets, here is the minimum JavaScript required:

~~~
#!javascript
var ws = new WebSocket("ws://localhost:9999/");

ws.onmessage = function (e) {
  gotMessage(e.data);
};

var gotMessage = function(serialised_json) { ... };
~~~

Note that `gotMessage` is a function that you should define, which receives a monto product. Note that the argument will be a string, so use `json.parse()` to decode the product string into a structured (JSON) form.

The above code will give you basic functionality for receiving a (serialised) JSON *monto message*, which includes information about the source of the product, the language, the product type, and other product metadata. If you want your message to receive only the generated product (without the metadata), you can use the following method, provided by `jsmonto.js`, which creates the WebSocket connection and extracts the product contents for you:

~~~
#!javascript
MontoSink(gotMessage);
~~~

If you want to only process products of a certain language or product type, you can pass `MontoSink` a set of properties instead of just a single function, like so:

~~~
#!javascript
MontoSink({
    receive:      gotMessage,
    contentsonly: true,
    deserialise:  true,
    language:     'json',
    product:      ['ast', 'tree'],
});
~~~

The properties available are:

 - `receive`  (required) A callback function that takes one argument, which will be evoked whenever an appropriate monto product is received.

 - `host` (default `"localhost"`) The host address of the machine running `pyjsmonto.py`

 - `port` (default `9999`) The port that `pyjsmonto.py` is broadcasting on.
 
 - `contentsonly` (default `true`) If set to true, `receive` will be passed only the 'contents' field of the monto message. This is usually the most important part of the message, but set this to false if you also want access to the metadata associated with the product contents.

 - `deserialise` (default `false`) If true, the contents will be deserialised (into JSON) before being passed to `receive`. This will override the `contentsonly` property to true.

 - `language` (default `[]`) If a single string is provided, only products with a language matching\* that string will be passed to `receive`. If an array of strings is provided, only products with a language matching\* one of the array's members will be passed. If anything else is provided, products of any language will be passed.

 - `product` (default []) Similarly to `language`, provide a string or array of strings to filter which `product` messages will be passed to `receive`.

 - `raw` (default `false`) If set to true, `receive` will be passed the *raw* message received from `pyjsmonto.py`. This is the stringified JSON message containing the product and all of its metadata. This overrides the properties `contentsonly`, `deserialise`, `language`, and `product`.

 - `debug` (default `false`) If true, debug logging will be sent to the console.

 \* All `language` and `product` comparisons are case insensitive.


Frontends
---------
Four JavaScript sinks frontends are available in the [examples](examples) directory. These perform the following functions:

### Basic HTML
`montohtml.html` takes any product of the language `html` and refreshes the existing page to show that HTML.

### Markdown
`montomarkdown.html` takes any product of the language `markdown` and renders it in HTML, using [marked](https://github.com/chjj/marked) to generate the HTML and CSS styling from [this repo](https://github.com/jasonm23/markdown-css-themes).

In the following screenshot, [SublimeMonto](https://bitbucket.org/inkytonik/sublimemonto) is being used as a source, and the [reflect.py](https://bitbucket.org/inkytonik/monto/src/ea78f8820d05/python/reflect.py) example server is used to forward `markdown` versions as products to all sinks.

![A screenshot of the markdown frontend in use.](https://bitbucket.org/repo/nor65o/images/217091487-markdown_example.png)

### D3 Trees
`d3ast.html` takes a product with language `json` and product name `ast`, in the following format representing a tree, and renders a tree using a [D3.js](http://d3js.org/) [Tree Layout](https://github.com/mbostock/d3/wiki/Tree-Layout).

~~~
#!javascript
["MainClass", 
        ["IdnDef", 
            ["MyMain"]],
        ["MainMethod", 
            ["Println",
                ["Hello World"]]]]
~~~

In the following screenshot, [SublimeMonto](https://bitbucket.org/inkytonik/sublimemonto) is being used on the left as a source, and the [MontoMiniJava](https://bitbucket.org/inkytonik/montominijava) server converts `java` (minijava) versions into `json`/`ast` products (among others). On the right, `d3ast.html` in Chrome receives these products and renders them using D3.js.


![A screenshot of the d3ast frontend in use.](https://bitbucket.org/repo/nor65o/images/312800037-tree_example.png)




### D3 Graphs
`d3astgraph.html` takes a product with language `json` and product name `astgraph`, in the following format representing a graph, and renders a graph using a [D3.js](http://d3js.org/) [Force Layout](https://github.com/mbostock/d3/wiki/Force-Layout).

~~~
#!javascript
[
    ["nodeA", [
        "nodeB",
        "nodeC"]],
    ["nodeB", [
        "nodeC"]],
    ["nodeC", []]
]
~~~

In the following screenshot, [SublimeMonto](https://bitbucket.org/inkytonik/sublimemonto) is being used on the left as a source, and the DataFlowGraph server (not yet published) converts any valid source versions into `json`/`astgraph` products (among others). On the right, `d3astgraph.html` in Chrome receives these products and renders them using D3.js.

![A screenshot of the d3astgraph frontend in use.](https://bitbucket.org/repo/nor65o/images/1651669569-graph_example.png)