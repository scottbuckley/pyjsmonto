#!/usr/bin/env python3

from ws4py.server.wsgirefserver import WSGIServer, WebSocketWSGIRequestHandler
from ws4py.server.wsgiutils import WebSocketWSGIApplication
from wsgiref.simple_server import make_server
from ws4py.websocket import WebSocket
from threading import Thread
from os.path import basename
import montolib
import json


WS_PORT = 9999

# a list of languages to pass on. leave empty to forward all products.
languagesToPassOn = ["json", "markdown", "html"]



#     #
##   ##  ####  #    # #####  ####
# # # # #    # ##   #   #   #    #
#  #  # #    # # #  #   #   #    #
#     # #    # #  # #   #   #    #
#     # #    # #   ##   #   #    #
#     #  ####  #    #   #    ####

def thread_montoSink():
    mnPrint("Serving a Monto sink...")
    montolib.sink (monto_receive, True)

def monto_receive (product):
    # process input product
    deets = json.loads(product)
    mnPrint("Received '", deets["product"], "' of ", basename(deets["source"]))

    # pass on prouct to websockets if applicable
    if (languageOfInterest(deets["language"])):
        mnPrint("Forwarding '", deets["product"], "' of ", basename(deets["source"]))
        sendToWebSockets(product)

    return True

def languageOfInterest (language):
    # return true if 'language' is a language we want to pass on to the websockets
    return ((languagesToPassOn == []) or (language in languagesToPassOn))



#     #                #####
#  #  # ###### #####  #     #  ####   ####  #    # ###### #####
#  #  # #      #    # #       #    # #    # #   #  #        #
#  #  # #####  #####   #####  #    # #      ####   #####    #
#  #  # #      #    #       # #    # #      #  #   #        #
#  #  # #      #    # #     # #    # #    # #   #  #        #
 ## ##  ###### #####   #####   ####   ####  #    # ######   #

# a gloabl list of the clients connected to the websocket server
websockets = {}

class MontoWebSocket(WebSocket):
    peerAddr = None

    def received_message(self, message):
        # when we receive a message from the (probably JS) WS client (do nothing)
        wsPrint("Received '", message, "' from ", self.peerAddr)

    def opened(self):
        # when a new (probably JS) WS client connects
        self.peerAddr = "{0}:{1}".format(self.peer_address[0], self.peer_address[1])
        wsPrint("New connection from ", self.peerAddr)

    def closed(self, code, reason=None):
        # when the (probably JS) WS client disconnects
        wsPrint("Closed connection with ", self.peerAddr)

def thread_webSocketServer():
    global websockets

    # set up a websocket server, and keep track of the connected clients
    server = make_server('', WS_PORT, server_class=WSGIServer,
                         handler_class=WebSocketWSGIRequestHandler,
                         app=WebSocketWSGIApplication(handler_cls=MontoWebSocket))
    server.initialize_websockets_manager()
    websockets = server.manager.websockets

    # start the server
    wsPrint("Serving WebSocket on port ", WS_PORT, "...")
    server.serve_forever()

def sendToWebSockets(message):
    # send 'message' to all existing websockets
    for key in websockets:
        ws = websockets[key]
        ws.send(message)







#     #
#     # ###### #      #####  ###### #####   ####
#     # #      #      #    # #      #    # #
####### #####  #      #    # #####  #    #  ####
#     # #      #      #####  #      #####       #
#     # #      #      #      #      #   #  #    #
#     # ###### ###### #      ###### #    #  ####

def wsPrint(*t):
    print("[WSOCK]", "".join([str(a) for a in t]))

def mnPrint(*t):
    print("[MONTO]", "".join([str(a) for a in t]))








#     #
##   ##   ##   # #    #
# # # #  #  #  # ##   #
#  #  # #    # # # #  #
#     # ###### # #  # #
#     # #    # # #   ##
#     # #    # # #    #

def startThread(thread_func):
    t = Thread(target=thread_func)
    t.start()


if __name__ == "__main__":
    print('')
    startThread(thread_webSocketServer)
    startThread(thread_montoSink)
