#!/usr/bin/env python3

from setuptools import setup

setup(name='PyJSMonto',
      version='0.4',
      description='Monto sink for browsers via WebSocket.',
      author='Scott Buckley',
      author_email='scott.j.h.buckley@gmail.com',
      url='https://bitbucket.org/scottbuckley/pyjsmonto',
      install_requires=['Monto'],
      scripts=['pyjsmonto.py'])
