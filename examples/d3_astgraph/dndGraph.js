// set up some viewer size info
var width  = document.body.offsetWidth;
var height = document.body.offsetHeight;

// for keeping track of recurring nodes between updates
var nodemap = {};

// the d3 force object
var force = d3.layout.force()
    .charge(-1000)
    .linkDistance(100)
    .alpha(0.01)
    .size([width, height])
    .nodes([])
    .links([]);

// the svg element on the page
var svg = d3.select("body").append("svg")
    .attr("width", width)
    .attr("height", height);

// set up the arrow-head (SVG Marker)
svg.append("defs").append("marker")
    .attr("id", "end")
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 20)
    .attr("refY", 0)
    .attr("markerWidth", 6)
    .attr("markerHeight", 6)
    .attr("orient", "auto")
    .append("path")
        .attr("d", "M0,-5L10,0L0,5");

// set up svg groups to contain links and nodes (for z-indexing)
svg.append("g")
    .attr("id", "g_links")
svg.append("g")
    .attr("id", "g_nodes")


// this class/function does all the rendering work
var drawTree = function(root) {
    this.update = function(source) {

        // update force.nodes, ensuring that references are safe
        force.nodes(source.nodes.map(function(n){
            if (!(nodemap[n.name]))
                nodemap[n.name] = n;
            return nodemap[n.name];
        }));

        force.links(source.links.map(function(l){
            return {
                source: nodemap[l.source],
                target: nodemap[l.target]
            };
        }));





        //  _ _       _
        // | (_)_ __ | | _____
        // | | | '_ \| |/ / __|
        // | | | | | |   <\__ \
        // |_|_|_| |_|_|\_\___/

        var link = svg
            .selectAll("#g_links").selectAll(".link")
                .data(force.links(), function(d){
                    // give a unique ID for each link
                    return d.source.name + " - " + d.target.name;
                });

        link.enter()
            .append("line")
                .attr("class", "link")
                .attr("marker-end", "url(#end)")

        link.exit().remove();




        //                  _
        //  _ __   ___   __| | ___  ___
        // | '_ \ / _ \ / _` |/ _ \/ __|
        // | | | | (_) | (_| |  __/\__ \
        // |_| |_|\___/ \__,_|\___||___/

        var node = svg
            .selectAll("#g_nodes").selectAll(".node")
                .data(force.nodes(), function(d){
                    // give a unique ID for each node
                    return d.name;
                });

        var enterNodes = node.enter()
            .append("g")
            .attr("class", "node")
            .call(force.drag)

        enterNodes
            .append("title")
            .text(function(d) { return d.name; });

        enterNodes
            .append("circle")
            .attr("r", 10)

        enterNodes
            .append("text")
            .attr("x", 12)
            .attr("dy", ".35em")
            .text(function(d) { return d.name; })

        node.exit().remove();




        //  _   _      _
        // | |_(_) ___| | __
        // | __| |/ __| |/ /
        // | |_| | (__|   <
        //  \__|_|\___|_|\_\

        force.on("tick", function() {
            link
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            node
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

        });



        // resume the simulation iff there are entering or exiting nodes or links
        var startOnce = function() {
            startOnce = function(){}; // this fn can only be called once
            force.start();
        };

        node.enter()[0].forEach(startOnce);
        node.exit()[ 0].forEach(startOnce);
        link.enter()[0].forEach(startOnce);
        link.exit()[ 0].forEach(startOnce);

    }

    // call update() on init
    this.update(root);
}