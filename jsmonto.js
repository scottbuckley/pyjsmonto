/* MontoSink can receive either
 * - A function. This is a callback that will be called with one argument (the
 * product contents), whenever a product is received.
 * - An object which specifies a number of settings. These settings include:
 *    - receive      *required*            (callback, as above)
 *    - host         (default "localhost") (host for WebSocket connection)
 *    - port         (default 9999)        (port for WebSocket connection)
 *    - raw          (default false)       (if true, the RAW monto message (text) will be sent to the callback)
 *    - contentsonly (default true)        (if true, only the 'contents' field of the monto message will be sent to the callback)
 *    - deserialise  (default false)       (if true, the 'contents' field will be (json) deserialised before being sent to the callback)
 *    - debug        (default false)       (if true, debug logging will be sent to the console)
 *    - language     (default [])          (a single string, or array of strings, representing the product languages that should be passed to the callback)
 *    - product      (default [])          (a single string, or array of strings, representing the product types that should be passed to the callback)
**/

function MontoSink(x) {
    if (isFunction(x)) {
        // a function was passed. use default settings.
        var ws = new WebSocket("ws://localhost:9999/");
        ws.onmessage = function(e) {
            // get data from received package
            var msg = e.data;

            // json-parse the data
            msg = safeJSON(msg);

            // pull out the contents
            msg = msg.contents || null;

            // call the passed function with the product contents.
            if (msg)
                x(msg);
        };
    }
    else {
        // an object was passed. process passed settings object / defaults.
        var receive      = x.receive;
        var host         = df(x.host,         "localhost");
        var port         = df(x.port,         "9999");
        var raw          = df(x.raw,          false);
        var contentsonly = df(x.contentsonly, true);
        var deserialise  = df(x.deserialise,  false);
        var debug        = df(x.debug,        false);
        var language     = safeListSetting(x.language);
        var product      = safeListSetting(x.product);

        // throw an error if receive was not passed. this is the only required 'parameter'
        if (!isFunction(x.receive)) throw new Error("You must specify a 'receive' function.");

        // debug logging
        function dbg() { if (debug) console.log.apply(console, arguments); return null;}
        dbg("montosink settings:", {host:host, port:port, raw:raw, contentsonly:contentsonly, deserialise:deserialise, debug:debug, language:language, product:product});

        // set up websocket object.
        var addr = "ws://" + host + ":" + port + "/";
        var ws = new WebSocket(addr);
        dbg("set up websocket at", addr, "(", ws, ")");

        ws.onmessage = function(e) {
            dbg("Received", e);
            var msg = e.data;

            // process the message, unless 'raw' is true
            if (!raw) {
                // json-parse the message
                msg = safeJSON(msg);

                // screen incorrect product types
                if (!matchList(msg.product, product)) return dbg("Product type did not match; discarding.");

                // screen incorrect product languages
                if (!matchList(msg.language, language)) return dbg("Language did not match; discarding.");

                // if requested, extract the contents
                if (contentsonly || deserialise)
                    msg = msg.contents || null;

                // if requested, json-parse the contents
                if (deserialise)
                    msg = safeJSON(msg) || null;
            }

            if (!msg) return dbg("There was an error processing the message as requested. This usually happens when json parsing fails.");

            // pass on the message
            dbg("Calling 'receive' with:",{argument:msg});
            receive(msg);
        };
    }
    return ws;
}


// return prop if it exists, otherwise return def
function df(prop, def) {
    if (typeof prop == 'undefined')
        return def;
    return prop;
}

// return true iff obj is a function
function isFunction(obj) {
  return !!(obj && obj.constructor && obj.call && obj.apply);
}

// return the json object parsed from jsonstring, or null if the parse fails
function safeJSON(jsonstring) {
  try {
      return JSON.parse(jsonstring);
  } catch(e) {
      return null;
  }
}

// return true if the item matches a settings-list, case-insensitive
// if list is empty, return true.
var matchList = function(item, list) {
    if (!item) return false;
    if (list.indexOf(item.toString().toLowerCase()) > -1) return true;
    if (list.length < 1) return true;
    return false;
}

// take a list or a string, and return a list of lowercase strings
function safeListSetting(setting) {
    if (Array.isArray(setting))
        return setting.map(function(s){return s.toLowerCase()});

    if (typeof setting == "string")
        return [setting.toLowerCase()];

    return [];
}